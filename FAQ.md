## NCGDMW Lua Edition Frequently Asked Questions

#### Q) How can I make my mod compatible with NCGDMW Lua Edition?

A) Generally speaking, NCGDMW Lua Edition is compatible with most things out of
   the box.

   I have seen mods in the wild that modify base attributes and/or skills to
   provide buffs and etc, this will cause level ups/downs as those buffs are
   added/removed. This can be somewhat jarring for NCGDMW Lua Edition users.

   One way to do this that is more friendly to NCGDMW Lua Edition is to use a
   "Curse" instead of an "Ability" for your spell type. A Curse affects stats
   as a modifier and will be ignored by NCGDMW Lua Edition's calulations,
   whereas an Ability will modify the base stat.

   If you're writing a Lua mod, you can refer to the "Events & Interface"
   documentation (included with every NCGDMW Lua Edition zip file or
   [on the website](https://modding-openmw.gitlab.io/ncgdmw-lua/events_and_interface/))
   for information about how to hook in from the Lua side of things.

#### Q) My level progress isn't increasing, when will I level up?

A) At this time, mods can't change the base game user interface, and NCGD
   zeroes out the vanilla level progress.

   The NCGD Stats Menu will show your level and decay progress if that feature
   is enabled. To use the menu, set it to a key using the mod settings menu.

#### Q) When will decay kick in? I've played for many hours/waited for hundreds of days and nothing happened!

A) Even with the "fast" setting, decay takes a long time to kick in. Increasing
   your attributes (done by increasing skills) will significantly affect decay
   progress, which of course doesn't happen while just standing there which
   means you could potentially wait for thousands of days before seeing a decay
   that way.

#### Q) Why can't I raise attributes and skills past 100?

A) At this time, the OpenMW-Lua API simply doesn't allow a way to do it. When
   that time comes, I will add the "mastery" feature.

   NCGDMW Lua Edition does not try to limit any stat to 100, though. So it is
   possible to raise attributes past 100 through normal leveling.
