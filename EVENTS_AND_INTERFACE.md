## Events & Interface

This page describes how to hook into NCGDMW Lua Edition from other mods.

#### Events

There are no events at this time.

#### Interface

##### Examples

Some simple usage examples are featured below.

```lua
local I = require("openmw.interfaces")
local ui = require("openmw.ui")

local function maybeRaiseAcrobatics()
  if I.NCGDMW then
    local currrent = I.NCGDMW.Skill("acrobatics")
    -- This will trigger an on-screen message
    I.NCGDMW.Skill("acrobatics", current + 1)
  end
end

local function ncgdCurrentDecayRate()
  if I.NCGDMW then
    local msg = "NCGDMW Decay Rate: %s"
    local rate = I.NCGDMW.DecayRate(nil, true)
    ui.showMessage(string.format(msg, rate))
  end
end

local function ncgdLevelProgress()
  if I.NCGDMW then
    local msg = "NCGDMW Level Progress: %s"
    local prog = I.NCGDMW.LevelProgress()
    ui.showMessage(string.format(msg, prog))
  end
end
```

##### `DecayRate()`

Args: `val, asStr`

When called with no args, gives the current decay rate.

Give it a number (arg `val`) 0 through 3 to set a new decay rate.

Call it like `DecayRate(nil, true)` to get the string representaion of the current decay rate.

##### `GrowthRate()`

Args: `val, asStr`

When called with no args, gives the current growth rate.

Give it a number (arg `val`) 0 through 3 to set a new growth rate.

Call it like `GrowthRate(nil, true)` to get the string representaion of the current growth rate.

##### `Attribute()`

Args: `name, val`

Give it just the `name` arg to get the value of that attribute.

Give it `name` as well as `val` to set the given attribute to the given value.

##### `LevelProgress()`

Returns the current level progress as a string representation of a number with a percent sign.

##### `Skill()`

Args: `name, val`

Give it just the `name` arg to get the value of that skill.

Give it `name` as well as `val` to set the given skill to the given value.
