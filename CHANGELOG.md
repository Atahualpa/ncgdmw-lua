## NCGDMW Lua Edition Changelog

#### beta4

* Support external changes to attributes (for instance if another mod raises them, or if the player becomes a vampire) on top of NCGDMW's own calculations
* Fixed a bug that caused extremely rapid decay rates
* Added a note about mod compatibility with NCGDMW Lua Edition and Ability vs Curse spell types
* Updated the FAQ entry about going past 100 to clarify the current state of that

<!-- [Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/TODO) -->

#### beta3

* Support new YAML-formatted localizations
* Added localizations for BR
* Added an interface for other mods to interact with NCGDMW
* Added documentation for events and the interface to the website
* Updated the mod settings menu to use the new builtin renderers provided by OpenMW
* The decay stats menu has a new layout and displays additional information
* The stats menu (with or without decay) will now display your [Marksman's Eye](https://modding-openmw.gitlab.io/marksmans-eye/) level, if you're also using that mod

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/7081151)

#### beta2

* Fixed a crash on skill up ([#1](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/1))
* Fixed health adjustments not applying after saving and reloading ([#2](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/2))
* Changed the layout of the zip file a bit
* Added changelog and faq pages to the website

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/6074455)

#### beta1

Initial public release, now compatible with the latest code in OpenMW 0.48 dev builds.

No new features were added for this release; the only changes involved porting code for updated APIs.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/5829792)

#### alpha1

Initial release of the mod, written for WIP code that had not yet been merged into OpenMW.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/5356693)
